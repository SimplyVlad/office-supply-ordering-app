﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeSupplyBLL
{
    public static class DALUtility
    {
        public static string GetSQLConnection(string name)
        {
            //Assume failure.
            string returnValue = null;
            ConnectionStringSettings settings 
                = ConfigurationManager.ConnectionStrings[name];

            //If found, return the connection string.
            if(settings != null)
            {
                returnValue = settings.ConnectionString;
            }
            return returnValue;
        }
    }
}
