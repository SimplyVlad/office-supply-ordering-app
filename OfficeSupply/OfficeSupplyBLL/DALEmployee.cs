﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace OfficeSupplyBLL
{
    class DALEmployee
    {
        public int LogIn(string userName, string password)
        {
            string connString = DALUtility.GetSQLConnection("OSConnection");
            using (SqlConnection conn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "Select EmployeeID from Employee where "
                        + "UserName = @UserName and Password = @Password ";
                    cmd.Parameters.AddWithValue("@UserName", userName);
                    cmd.Parameters.AddWithValue("Password", password);
                    int userId;
                    conn.Open();
                    userId = (int)cmd.ExecuteScalar();
                    if(userId > 0)
                    {
                        return userId;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
        }
    }
}
